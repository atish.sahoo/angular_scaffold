require 'test_helper'
require 'generators/angular_scaffold/install/install_generator'

module AngularScaffold
  class AngularScaffold::InstallGeneratorTest < Rails::Generators::TestCase
    tests AngularScaffold::InstallGenerator
    destination Rails.root.join('tmp/generators')
    setup :prepare_destination

    # test "generator runs without errors" do
    #   assert_nothing_raised do
    #     run_generator ["arguments"]
    #   end
    # end
  end
end
