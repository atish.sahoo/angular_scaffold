$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "angular_scaffold/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "angular_scaffold"
  s.version     = AngularScaffold::VERSION
  s.authors     = ["Atish Kumar Sahoo"]
  s.email       = ["atish.sahoo@ajatus.co.in"]
  s.homepage    = "https://gitlab.com/atish.sahoo/angular_scaffold"
  s.summary     = "Rails Angularjs scaffolding."
  s.description = "Angularjs view generation for rails application in quick way."
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails", "~> 4.2.6"

  s.add_development_dependency "sqlite3"
end
