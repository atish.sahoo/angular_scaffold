module AngularScaffold
	class InstallGenerator < Rails::Generators::Base
	  source_root File.expand_path('../templates', __FILE__)

	  attr_reader :app_name

	  def install_gems

	  	@app_name = Rails.application.class.parent_name

	  	if(File.exist?('bower.json'))
	  		remove_file 'bower.json'
	  	end

	  	append_to_file 'Gemfile',"\n\ngem 'bower-rails', '~> 0.10.0'"
	  	run "bundle install"
	  	run "rails g bower_rails:initialize json"

	  	if(File.exist?('bower.json'))
	  		remove_file 'bower.json'
	  	end
	  	copy_file 'bower.json','bower.json'
	  	run 'rake bower:install'

	  	empty_directory "app/assets/javascripts/angular-app"
	  	empty_directory "app/assets/javascripts/angular-app/controller"
	  	empty_directory "app/assets/javascripts/angular-app/directive"
	  	empty_directory "app/assets/javascripts/angular-app/filter"
	  	empty_directory "app/assets/javascripts/angular-app/service"
	  	empty_directory "app/assets/templates"

	  	if File.exist?("app/assets/javascripts/application.js")
	  		remove_file 'app/assets/javascripts/application.js'
	  	end

	  	copy_file "myangular/application.js", "app/assets/javascripts/application.js"

	  	if File.exist?("app/assets/javascripts/angular-app/app.js")
	  		remove_file 'app/assets/javascripts/angular-app/app.js'
	  	end

	  	copy_file "myangular/app.js", "app/assets/javascripts/angular-app/app.js.erb"
	  	copy_file "myangular/welcome_controller.js", "app/assets/javascripts/angular-app/controller/welcome_controller.js"

	  	insert_into_file "app/assets/javascripts/angular-app/app.js.erb", @app_name, before: 'Client'
	  	insert_into_file "app/assets/javascripts/angular-app/controller/welcome_controller.js", @app_name, before: 'Client'

	  	remove_file "public/index.html"
      	uncomment_lines 'config/routes.rb', /root 'welcome#index'/
      	run "rails g controller welcome index"


      	copy_file "AngularJS-medium.png", "app/assets/images/AngularJS-medium.png"
      	copy_file 'favicon.ico', "app/assets/images/favicon.ico"
      	empty_directory "app/assets/templates"
      	empty_directory "app/assets/templates/welcome"


      	copy_file "index_welcome.html.erb", "app/assets/templates/welcome/index.html.erb"

      	template "myangular/application.html.erb", "app/views/layouts/application.html.erb"

      	directory "fonts", "app/assets/fonts/"
        directory "fontawesome", "app/assets/stylesheets/fontawesome/"
        directory "bootstrap/css", "app/assets/stylesheets/bootstrap/"
        directory "bootstrap/js", "app/assets/javascripts/bootstrap/"

	    @application_css_file ='app/assets/stylesheets/application.css'
	    if (!(File.exist?('app/assets/stylesheets/application.css')) &&
	        File.exist?('app/assets/stylesheets/application.css.scss'))
	        @application_css_file ='app/assets/stylesheets/application.css.scss'
	    elsif !File.exist?('app/assets/stylesheets/application.css')
	        create_file @application_css_file
	    end

	    insert_into_file @application_css_file," *= require bootstrap/bootstrap.min.css\n", :after => "require_self\n"
	    insert_into_file @application_css_file," *= require bootstrap/bootstrap-responsive.min.css\n",:after => "bootstrap.min.css\n"
	    insert_into_file @application_css_file," *= require fontawesome/font-awesome.css\n",:after => "bootstrap-responsive.min.css\n"
	    insert_into_file @application_css_file," *= require bootstrap/bootstrap-theme.min.css\n",:after => "font-awesome.css\n"

	    insert_into_file "app/assets/javascripts/application.js","//= require_tree ./bootstrap/\n", before: "//= require_tree ."
	  end

	#   def init_angular_scaffold
		  

	#       @application_css_file ='app/assets/stylesheets/application.css'
	#       if (!(File.exist?('app/assets/stylesheets/application.css')) &&
	#           File.exist?('app/assets/stylesheets/application.css.scss'))
	#         @application_css_file ='app/assets/stylesheets/application.css.scss'
	#       elsif !File.exist?('app/assets/stylesheets/application.css')
	#         create_file @application_css_file
	#       end

	#       directory "underscore", "app/assets/javascripts/underscore/"
	#       directory "angularjs", "app/assets/javascripts/angularjs/"
	#       directory "fonts", "app/assets/fonts/"
 #          directory "fontawesome", "app/assets/stylesheets/fontawesome/"
 #          directory "bootstrap/css", "app/assets/stylesheets/bootstrap/"
 #          directory "bootstrap/js", "app/assets/javascripts/bootstrap/"
 #          directory "bootstrap/img", "app/assets/javascripts/img/"

 #          if File.exist?('app/assets/javascripts/application.js')
	#         insert_into_file "app/assets/javascripts/application.js",
	#           "//= require_tree ./angularjs/\n", :after => "jquery_ujs\n"
	#         insert_into_file "app/assets/javascripts/application.js",
 #            "//= require_tree ./underscore/\n", after: "angularjs/\n"
	#         insert_into_file "app/assets/javascripts/application.js",
 #            "//= require_tree ./bootstrap/\n", after: "underscore/\n"
	#       else
	#         copy_file "application.js", "app/assets/javascripts/application.js"
	#       end

	#       @application_css_file ='app/assets/stylesheets/application.css'
	#       if (!(File.exist?('app/assets/stylesheets/application.css')) &&
	#           File.exist?('app/assets/stylesheets/application.css.scss'))
	#         @application_css_file ='app/assets/stylesheets/application.css.scss'
	#       elsif !File.exist?('app/assets/stylesheets/application.css')
	#         create_file @application_css_file
	#       end

	#       insert_into_file @application_css_file," *= require bootstrap/bootstrap.min.css\n", :after => "require_self\n"
 #          insert_into_file @application_css_file," *= require bootstrap/bootstrap-responsive.min.css\n",:after => "bootstrap.min.css\n"
 #          insert_into_file @application_css_file," *= require fontawesome/font-awesome.css\n",:after => "bootstrap-responsive.min.css\n"
 #          insert_into_file @application_css_file," *= require bootstrap/bootstrap-theme.min.css\n",:after => "font-awesome.css\n"

	#       insert_into_file "config/application.rb", "\n\tconfig.generators do |g| \n\t\tg.assets false \n\t\tg.helper false \n\t\tg.test_framework false\n\tend\n\n\tconfig.time_zone = 'Kolkata'\n", after: "config.active_record.raise_in_transactional_callbacks = true\n"

	#       # if options["no-jquery"]
	#       #   gsub_file "app/assets/javascripts/application.js",
	#       #     /\/\/= require jquery_ujs\n/, ''
	#       #   gsub_file "app/assets/javascripts/application.js",
	#       #     /\/\/= require jquery\n/, ''
	#       #   #gsub_file "app/assets/javascripts/application.js",
	#       #   #  /^$\n/, ''
	#       # end
	#   end

	#   attr_reader :app_name, :container_class, :language

	#   def set_angular_app
	#   	@app_name = Rails.application.class.parent_name

	#   	empty_directory "app/assets/javascripts/angular_app"
	#   	empty_directory "app/assets/javascripts/angular_app/controller"
	#   	empty_directory "app/assets/javascripts/angular_app/directive"
	#   	empty_directory "app/assets/javascripts/angular_app/filter"
	#   	empty_directory "app/assets/javascripts/angular_app/service"
	#   	empty_directory "app/assets/templates"

	#   	copy_file "routes.js.erb", "app/assets/javascripts/angular_app/routes.js.erb"

	#   	insert_into_file "app/assets/javascripts/angular_app/routes.js.erb", @app_name, before: 'Client'

	#   	template "application.html.erb", "app/views/layouts/application.html.erb"

	#   end

	#   def generate_welcome_controller
	#       remove_file "public/index.html"
	#       uncomment_lines 'config/routes.rb', /root 'welcome#index'/
	#       run "rails g controller welcome index"


	#       copy_file "AngularJS-medium.png", "app/assets/images/AngularJS-medium.png"
	#       copy_file 'favicon.ico', "app/assets/images/favicon.ico"
	#       empty_directory "app/assets/templates"
	#       empty_directory "app/assets/templates/welcome"


	#       copy_file "index_welcome.html.erb", "app/assets/templates/welcome/index.html.erb"


	# 	  # javascript

	#         ['csrf', 'welcome'].each do |prefix|
	#           copy_file "#{prefix}_controller.js","app/assets/javascripts/angular_app/controller/#{prefix}_controller.js"
	#         end
	#         # insert_into_file "app/assets/javascripts/welcome_controller.js", @app_name, before: 'Client'

	#       # append_to_file "app/assets/javascripts/application.js",
	#       #   "//= require routes\n"
	#       # append_to_file "app/assets/javascripts/application.js",
	#       #   "//= require welcome_controller\n"
	#       # append_to_file @application_css_file,
	#       #   ".center {text-align: center;}\n"
	#       insert_into_file "app/controllers/application_controller.rb",
	# %{
	#   private

	#   # AngularJS automatically sends CSRF token as a header called X-XSRF
	#   # this makes sure rails gets it
	#   def verified_request?
	#     !protect_against_forgery? || request.get? ||
	#       form_authenticity_token == params[request_forgery_protection_token] ||
	#       form_authenticity_token == request.headers['X-CSRF-Token'] ||
	#       form_authenticity_token == (request.headers['X-XSRF-Token'].chomp('"').reverse.chomp('"').reverse if request.headers['X-XSRF-Token'])
	#   end
	#  }, before: "end"

	#     end
	end
end
