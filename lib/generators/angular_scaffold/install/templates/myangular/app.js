'use strict';

/**
 * @ngdoc overview
 * @name trackingAdminApp
 * @description
 * # trackingAdminApp
 *
 * Main module of the application.
 */

angular
	.module('Client', [
	    'ngCookies',
	    'ngResource',
	    'ngRoute'
	])
	.config(['$locationProvider','$routeProvider','$httpProvider',function($locationProvider,$routeProvider,$httpProvider) {
	
		$httpProvider.defaults.headers.common['X-CSRF-Token'] =$('meta[name=csrf-token]').attr('content');
		
		$locationProvider.html5Mode({
			enabled: true,
  			requireBase: false
		});

	 	$routeProvider
	 	.when("/", {
	        controller: "WelcomeController",
	        templateUrl: "<%= asset_path('welcome/index.html') %>"
	    }).otherwise({
	        redirectTo: "/"
	    });
	}]);