module AngularScaffold
  class Engine < ::Rails::Engine
    isolate_namespace AngularScaffold
  end
end
